#
# Copyright 2019, PR-OS
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

##

##
##
# Bootanimation
ifeq ($(TARGET_SCREEN_WIDTH),)
    $(warning TARGET_SCREEN_WIDTH undefined, using 1080p)
    TARGET_SCREEN_WIDTH := 1080
endif
ifeq ($(shell [[ $(TARGET_SCREEN_WIDTH) -gt 1080 ]] && echo $$?),0)
    PRODUCT_COPY_FILES += \
        vendor/ganja/prebuilts/media/bootanimation/bootanimation1440.zip:system/media/bootanimation.zip
else
    PRODUCT_COPY_FILES += \
        vendor/ganja/prebuilts/media/bootanimation/bootanimation1080.zip:system/media/bootanimation.zip
endif
