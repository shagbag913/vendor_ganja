##
# Google sounds extracted from sailfish Q DP1

PRODUCT_COPY_FILES += \
    vendor/ganja/prebuilts/media/audio/alarms/Rise.ogg:system/media/audio/alarms/Rise.ogg \
    vendor/ganja/prebuilts/media/audio/alarms/Drip.ogg:system/media/audio/alarms/Drip.ogg \
    vendor/ganja/prebuilts/media/audio/alarms/Flow.ogg:system/media/audio/alarms/Flow.ogg \
    vendor/ganja/prebuilts/media/audio/alarms/Sway.ogg:system/media/audio/alarms/Sway.ogg \
    vendor/ganja/prebuilts/media/audio/alarms/Wag.ogg:system/media/audio/alarms/Wag.ogg \
    vendor/ganja/prebuilts/media/audio/alarms/Nudge.ogg:system/media/audio/alarms/Nudge.ogg \
    vendor/ganja/prebuilts/media/audio/alarms/Orbit.ogg:system/media/audio/alarms/Orbit.ogg \
    vendor/ganja/prebuilts/media/audio/alarms/Bounce.ogg:system/media/audio/alarms/Bounce.ogg \
    vendor/ganja/prebuilts/media/audio/alarms/Awaken.ogg:system/media/audio/alarms/Awaken.ogg \
    vendor/ganja/prebuilts/media/audio/alarms/Gallop.ogg:system/media/audio/alarms/Gallop.ogg \
    vendor/ganja/prebuilts/media/audio/notifications/Strum.ogg:system/media/audio/notifications/Strum.ogg \
    vendor/ganja/prebuilts/media/audio/notifications/Chime.ogg:system/media/audio/notifications/Chime.ogg \
    vendor/ganja/prebuilts/media/audio/notifications/Flick.ogg:system/media/audio/notifications/Flick.ogg \
    vendor/ganja/prebuilts/media/audio/notifications/Hey.ogg:system/media/audio/notifications/Hey.ogg \
    vendor/ganja/prebuilts/media/audio/notifications/Clink.ogg:system/media/audio/notifications/Clink.ogg \
    vendor/ganja/prebuilts/media/audio/notifications/Trill.ogg:system/media/audio/notifications/Trill.ogg \
    vendor/ganja/prebuilts/media/audio/notifications/Birdsong.ogg:system/media/audio/notifications/Birdsong.ogg \
    vendor/ganja/prebuilts/media/audio/notifications/Note.ogg:system/media/audio/notifications/Note.ogg \
    vendor/ganja/prebuilts/media/audio/ui/ChargingStarted.ogg:system/media/audio/ui/ChargingStarted.ogg \
    vendor/ganja/prebuilts/media/audio/ui/Trusted.ogg:system/media/audio/ui/Trusted.ogg \
    vendor/ganja/prebuilts/media/audio/ui/NFCInitiated.ogg:system/media/audio/ui/NFCInitiated.ogg \
    vendor/ganja/prebuilts/media/audio/ui/camera_click.ogg:system/media/audio/ui/camera_click.ogg \
    vendor/ganja/prebuilts/media/audio/ui/InCallNotification.ogg:system/media/audio/ui/InCallNotification.ogg \
    vendor/ganja/prebuilts/media/audio/ui/Undock.ogg:system/media/audio/ui/Undock.ogg \
    vendor/ganja/prebuilts/media/audio/ui/VideoStop.ogg:system/media/audio/ui/VideoStop.ogg \
    vendor/ganja/prebuilts/media/audio/ui/VideoRecord.ogg:system/media/audio/ui/VideoRecord.ogg \
    vendor/ganja/prebuilts/media/audio/ui/KeypressDelete.ogg:system/media/audio/ui/KeypressDelete.ogg \
    vendor/ganja/prebuilts/media/audio/ui/NFCTransferComplete.ogg:system/media/audio/ui/NFCTransferComplete.ogg \
    vendor/ganja/prebuilts/media/audio/ui/NFCFailure.ogg:system/media/audio/ui/NFCFailure.ogg \
    vendor/ganja/prebuilts/media/audio/ui/Effect_Tick.ogg:system/media/audio/ui/Effect_Tick.ogg \
    vendor/ganja/prebuilts/media/audio/ui/audio_initiate.ogg:system/media/audio/ui/audio_initiate.ogg \
    vendor/ganja/prebuilts/media/audio/ui/NFCSuccess.ogg:system/media/audio/ui/NFCSuccess.ogg \
    vendor/ganja/prebuilts/media/audio/ui/WirelessChargingStarted.ogg:system/media/audio/ui/WirelessChargingStarted.ogg \
    vendor/ganja/prebuilts/media/audio/ui/Unlock.ogg:system/media/audio/ui/Unlock.ogg \
    vendor/ganja/prebuilts/media/audio/ui/KeypressStandard.ogg:system/media/audio/ui/KeypressStandard.ogg \
    vendor/ganja/prebuilts/media/audio/ui/audio_end.ogg:system/media/audio/ui/audio_end.ogg \
    vendor/ganja/prebuilts/media/audio/ui/LowBattery.ogg:system/media/audio/ui/LowBattery.ogg \
    vendor/ganja/prebuilts/media/audio/ui/NFCTransferInitiated.ogg:system/media/audio/ui/NFCTransferInitiated.ogg \
    vendor/ganja/prebuilts/media/audio/ui/camera_focus.ogg:system/media/audio/ui/camera_focus.ogg \
    vendor/ganja/prebuilts/media/audio/ui/Dock.ogg:system/media/audio/ui/Dock.ogg \
    vendor/ganja/prebuilts/media/audio/ui/KeypressReturn.ogg:system/media/audio/ui/KeypressReturn.ogg \
    vendor/ganja/prebuilts/media/audio/ui/KeypressSpacebar.ogg:system/media/audio/ui/KeypressSpacebar.ogg \
    vendor/ganja/prebuilts/media/audio/ui/KeypressInvalid.ogg:system/media/audio/ui/KeypressInvalid.ogg \
    vendor/ganja/prebuilts/media/audio/ui/Lock.ogg:system/media/audio/ui/Lock.ogg \
    vendor/ganja/prebuilts/media/audio/ringtones/Rrrring.ogg:system/media/audio/ringtones/Rrrring.ogg \
    vendor/ganja/prebuilts/media/audio/ringtones/Zen.ogg:system/media/audio/ringtones/Zen.ogg \
    vendor/ganja/prebuilts/media/audio/ringtones/Summer_night.ogg:system/media/audio/ringtones/Summer_night.ogg \
    vendor/ganja/prebuilts/media/audio/ringtones/Hey_hey.ogg:system/media/audio/ringtones/Hey_hey.ogg \
    vendor/ganja/prebuilts/media/audio/ringtones/Romance.ogg:system/media/audio/ringtones/Romance.ogg \
    vendor/ganja/prebuilts/media/audio/ringtones/Early_bird.ogg:system/media/audio/ringtones/Early_bird.ogg \
    vendor/ganja/prebuilts/media/audio/ringtones/Shooting_star.ogg:system/media/audio/ringtones/Shooting_star.ogg \
    vendor/ganja/prebuilts/media/audio/ringtones/Spaceship.ogg:system/media/audio/ringtones/Spaceship.ogg \
    vendor/ganja/prebuilts/media/audio/ringtones/Beats.ogg:system/media/audio/ringtones/Beats.ogg \
    vendor/ganja/prebuilts/media/audio/ringtones/Dance_party.ogg:system/media/audio/ringtones/Dance_party.ogg
