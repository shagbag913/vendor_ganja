# Build date: monthDay-hourMinute (Ex 03182021)
GANJA_BUILD_DATE := $(shell date +'%m%d%H%M')

##
# Version number: bigVersion.smallVersion
#	bigVersion: increases on new Android version release
#	smallVersion: increases on new security patch level (monthly)
GANJA_VERSION_NUM := 4.20

##
# device codename
GANJA_DEVICE := $(subst ganja_,,$(TARGET_PRODUCT))

##
# Zip name: GANJA_<device>_<GANJA_VERSION_NUM>_<GANJA_BUILD_DATE>.zip
GANJA_ZIP_NAME := GANJA_$(TARGET_DEVICE)_$(GANJA_VERSION_NUM)_$(GANJA_BUILD_DATE).zip

##
# Overlays
PRODUCT_PACKAGE_OVERLAYS += vendor/ganja/overlay

##
# Exclude SysUI tests
EXCLUDE_SYSTEMUI_TESTS := true


##
# Google sounds
$(call inherit-product, vendor/ganja/prebuilts/media/google-audio.mk)

##
# Packages
PRODUCT_PACKAGES += \
    Lawnchair

##
# priv-app / hidden api whitelist
PRODUCT_COPY_FILES += \
    vendor/ganja/prebuilts/etc/permissions/privapp-permissions-pr.xml:system/etc/permissions/privapp-permissions-pr.xml \
    vendor/ganja/prebuilts/etc/sysconfig/pr-hiddenapi-package-whitelist.xml:system/etc/sysconfig/pr-hiddenapi-package-whitelist.xml
